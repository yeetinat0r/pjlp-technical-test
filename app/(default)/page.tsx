export const metadata = {
  title: 'Home - PJLP Technical Test',
  description: 'React Landing Pages',
}

import Main from '@/components/main'
import Announcement from '@/components/announcement'
import About from '@/components/about'
import Maintenance from '@/components/maintenance'

export default function Home() {
  return (
    <>
      <Main />
      <Announcement />
      <About />
      <Maintenance />
    </>
  )
}
