export const metadata = {
  title: 'Maintenance - PJLP Technical Test',
  description: 'React Landing Pages',
}

export default function Maintenance() {
  return (
    <section className="relative">
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="pt-32 pb-12 md:pt-40 md:pb-20">

          {/* Page header */}
          <div className="relative flex flex-col items-center" data-aos="fade-up" data-aos-delay="500" data-aos-anchor="[data-aos-id-blocks]">
            <svg className="w-16 h-16 mb-4" viewBox="0 0 64 64" xmlns="http://www.w3.org/2000/svg">
              <rect className="fill-current text-purple-600" width="64" height="64" rx="32" />
              <g transform="translate(21 22)" strokeLinecap="square" strokeWidth="2" fill="none" fillRule="evenodd">
                <path className="stroke-current text-purple-300" d="M17 2V0M19.121 2.879l1.415-1.415M20 5h2M19.121 7.121l1.415 1.415M17 8v2M14.879 7.121l-1.415 1.415M14 5h-2M14.879 2.879l-1.415-1.415" />
                <circle className="stroke-current text-purple-300" cx="17" cy="5" r="3" />
                <path className="stroke-current text-purple-100" d="M8.86 1.18C3.8 1.988 0 5.6 0 10c0 5 4.9 9 11 9a10.55 10.55 0 003.1-.4L20 21l-.6-5.2a9.125 9.125 0 001.991-2.948" />
              </g>
            </svg>
          </div>

          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-20">
            <h1 className="h1">Oops, seems like we are down right now.</h1>
          </div>

          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-20">
            <h4 className="h4">Please come back later</h4>
          </div>

        </div>
      </div>
    </section>
  )
}
