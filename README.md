# Description

This project is created for Kominfo PJLP Technical Test purposes only. A landing page using React.

## Contents

* Home Page (with Announcement and About sections)
* Maintenance Page

## Running the server

```bash
npm run dev
# or
yarn dev
```

## Credits

Credits to [https://github.com/cruip](Cruip) for the template.
