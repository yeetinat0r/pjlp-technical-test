export default function About() {
  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20 border-t border-gray-800">

          {/* Items */}
          <div className="grid gap-20">

            <div className="md:grid md:grid-cols-12 md:gap-6 items-center">
              {/* Content */}
              <div className="max-w-xl md:max-w-none md:w-full mx-auto md:col-span-7 lg:col-span-6" data-aos="fade-right">
                <div className="md:pr-4 lg:pr-12 xl:pr-16">
                  <div className="font-architects-daughter text-xl text-purple-600 mb-2">Always here to hear</div>
                  <h3 className="h3 mb-3">Contact Us</h3>
                  <ul className="text-lg text-gray-400 -mb-2">
                  <li className="flex items-center mb-2">
                    <span>abc@de.com</span>
                  </li>
                  <li className="flex items-center mb-2">
                    <span>+62-21-23456</span>
                  </li>
                  <li className="flex items-center">
                    <span>+62-811-2345-6789</span>
                  </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>
  )
}
