export default function Testimonials() {
  return (
    <section>
      <div className="max-w-6xl mx-auto px-4 sm:px-6">
        <div className="py-12 md:py-20 border-t border-gray-800">

          {/* Section header */}
          <div className="max-w-3xl mx-auto text-center pb-12 md:pb-20">
            <h2 className="h2 mb-4">Maintenance Page</h2>
            <div data-aos="fade-up" data-aos-delay="600">
              <a className="btn text-white bg-gray-700 hover:bg-gray-800 w-full sm:w-auto sm:ml-4" href="/maintenance">View Page</a>
            </div>
          </div>

        </div>
      </div>
    </section>
  )
}
